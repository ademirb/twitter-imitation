## For the first task of printing full names next to usernames:

1. In class User, I add new variable fullName ( We have to create every user, with Full Name variable)
2. In class TweetDTO, I add new variable full name ( This object, which is send through API, needs to contain fullName variable.
3. Fronted model which is used when fetching data, TweetModel also needs fullName variable
4. At the end, I created new column and display Full Name variable

## For the second task of printing number of Following, Followers and Posts

Backend
1. I created new DTO model in services repository, DataDTO
2. In UserController I made new get method to collect data, put it in new object DataDTO and send it on server
3. In UserServices are new methods for collecting data
3.1. I did not use existing method for collecting followers and following, because they take Principle as a parameter. Since, principle only work with logged users, I used username as a parameter to take data
4. I counted numbers in backend and strictly passed Integer to fronted, since that was the task. But this method could easily get modified to pass full list of followers and following users, if nessecary.

Frontend
1. In tweet.service.ts, I made new method to fetch data from backend
2. This data will be stored in new Interface Model, user.model.ts
3. In user-tweets-view-component I call function to collect data
4. Than pass this new variable to user-tweets-table and display it

##Bonus Task:

Frontend
1. New Component “Register”
2. New Model “NewUserModel”
3. New Service “Register Service”
Data from form is packed in NewUserModel. From there, method ‘register’ is called and it passed object user as a parameter. Method ‘register’ in Registration Service post data to backand.
If response is success, it navigate to login page.

Backend
1. New Controller for registration “RegistrationUser”
2. Post Mapping value for “newUser” method is /api/new/user
3. It requests object as a parameter
4. And It goes to new method, which saves user as an object.
5. In SecurityConfiguration File, I approved unauthenticated users (or new users) to post data on "/api/new/user"

