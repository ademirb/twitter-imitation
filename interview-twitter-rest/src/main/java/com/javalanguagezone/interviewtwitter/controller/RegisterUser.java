package com.javalanguagezone.interviewtwitter.controller;

import com.javalanguagezone.interviewtwitter.domain.User;
import com.javalanguagezone.interviewtwitter.repository.UserRepository;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("new")
public class RegisterUser {

  private UserRepository userRepository;

  public RegisterUser(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @CrossOrigin
  @PostMapping(value = "user")
  public void newUser(@RequestBody User user){
    saveUser(user);
  }

  public void saveUser(User user){
    User newU = userRepository.save(new User(user.getUsername(), user.getPassword(), user.getFullName()));
    userRepository.save(newU);
    System.out.println(newU);
  }
}
