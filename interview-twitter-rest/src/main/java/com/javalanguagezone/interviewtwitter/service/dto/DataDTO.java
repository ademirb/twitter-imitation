package com.javalanguagezone.interviewtwitter.service.dto;

public class DataDTO {
  private int followingNum;
  private int followersNum;
  private int postsNum;

  public DataDTO(int followingNum, int followersNum, int postsNum) {
    this.followingNum = followingNum;
    this.followersNum = followersNum;
    this.postsNum = postsNum;
  }

  public int getFollowingNum() {
    return followingNum;
  }

  public void setFollowingNum(int followingNum) {
    this.followingNum = followingNum;
  }

  public int getFollowersNum() {
    return followersNum;
  }

  public void setFollowersNum(int followersNum) {
    this.followersNum = followersNum;
  }

  public int getPostsNum() {
    return postsNum;
  }

  public void setPostsNum(int postsNum) {
    this.postsNum = postsNum;
  }
}
