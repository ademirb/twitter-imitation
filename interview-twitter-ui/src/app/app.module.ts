import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HttpClientModule} from "@angular/common/http";
import {AuthModule} from "./auth/auth.module";
import { RegisterComponent } from './register/register.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
  ],
    imports: [
        AppRoutingModule,
        AuthModule,
        BrowserModule,
        HttpClientModule,
        ReactiveFormsModule,
    ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
