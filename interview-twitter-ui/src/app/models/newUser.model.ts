export interface NewUserModel {
  fullName: string;
  username: string;
  password: string;
}
