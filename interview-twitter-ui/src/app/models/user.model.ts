export interface UserModel {
  followingNum: number,
  followersNum: number,
  postsNum: number
}
