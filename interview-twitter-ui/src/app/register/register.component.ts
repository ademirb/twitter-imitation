import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import { RegisterService } from '../services/register/register.service';
import {NewUserModel} from '../models/newUser.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [RegisterService]
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  newUser: NewUserModel;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private service: RegisterService
  ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onSubmit(form: NgForm) {
    this.newUser = form.value;
    console.log(this.newUser);
    form.resetForm();
    this.service.register(this.newUser).subscribe(
      res => {
        this.router.navigate(['/login']);
      },
      err => {
        console.log('error');
      }
    );
  }
}
