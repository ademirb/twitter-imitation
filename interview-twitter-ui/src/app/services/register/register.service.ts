import { Injectable } from '@angular/core';
import { NewUserModel} from '../../models/newUser.model';
import {HttpClient} from '@angular/common/http';

const ENDPOINT_BASE = 'http://localhost:8080/api/new/user';

@Injectable()
export class RegisterService {

  constructor(private http: HttpClient) { }

  // post new user
  register (user: NewUserModel) {
    return this.http.post(ENDPOINT_BASE, user);
  }
}
