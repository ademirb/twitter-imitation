import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TweetModel} from '../../models/tweet.model';
import {Observable} from 'rxjs/Observable';
import {UserModel} from '../../models/user.model';

const ENDPOINT_BASE = '/api/tweets';

@Injectable()
export class TweetService {

  constructor(private http: HttpClient) {
  }

  fetch(): Observable<TweetModel[]> {
    return this.http.get<TweetModel[]>(ENDPOINT_BASE);
  }

  fetchForUser(username: string) {
    return this.http.get<TweetModel[]>(ENDPOINT_BASE + '/' + username);
  }

  // Collecting user data of followers, following and posts
  fetchForFollowers(username: string) {
    return this.http.get<UserModel[]>('/api/' + username + '/data');
  }

  create(tweetContent: string) {
    console.log(this.http.post<TweetModel>(ENDPOINT_BASE, tweetContent));
    return this.http.post<TweetModel>(ENDPOINT_BASE, tweetContent);
  }

}
